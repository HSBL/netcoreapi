using System.Collections.Generic;

namespace MountrashMockAPI.ViewModel
{
  public class PaginatedListViewModel<T> where T : class
  {
    public PaginatedListViewModel(int pageIndex, int pageSize, long count, IEnumerable<T> listData)
    {
      PageIndex = pageIndex;
      PageSize = pageSize;
      Count = count;
      ListData = listData;
    }

    public IEnumerable<T> ListData { get; set; }

    public int PageIndex { get; }

    public int PageSize { get; }

    public long Count { get; }
  }

}
