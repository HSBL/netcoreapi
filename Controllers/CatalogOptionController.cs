using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using MountrashMockAPI.Infrastructure;
using MountrashMockAPI.ViewModel;
using MountrashMockAPI.Models;

namespace MountrashMockAPI.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CatalogOptionController : ControllerBase
  {
    private readonly DatabaseContext _context;

    public CatalogOptionController(DatabaseContext context)
    {
      _context = context;
    }

    // GET: api/CatalogOption
    [HttpGet]
    public async Task<IActionResult> GetCatalogOption([FromQuery]int pageSize = 10, [FromQuery]int pageIndex = 0, CancellationToken cancellationToken = default(CancellationToken))
    {

      try
      {
        var totalItems = await _context.CatalogOption
                .LongCountAsync();

        var itemsOnPage = await _context.CatalogOption
            .OrderBy(c => c.Name)
            .Skip(pageSize * pageIndex)
            .Take(pageSize)
            .ToListAsync();

        var model = new PaginatedListViewModel<CatalogOption>(pageIndex, pageSize, totalItems, itemsOnPage);

        return Ok(model);
      }
      catch (Exception ex)
      {
        return BadRequest(new { Message = ex.Message });
      }
    }

    // POST: api/CatalogOption
    // To protect from overposting attacks, enable the specific properties you want to bind to, for
    // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    [HttpPost]
    // public async Task<ActionResult<CatalogOption>> PostCatalogOption(CatalogOption catalogOption)
    // {
    //   _context.CatalogOption.Add(catalogOption);
    //   await _context.SaveChangesAsync();

    //   return CreatedAtAction("GetCatalogOption", new { id = catalogOption.Id }, catalogOption);
    // }
    public async Task<IActionResult> CreateCatalogOption([FromBody]CatalogOption option)
    {
      try
      {

        var item = new CatalogOption(option.CatalogSubType.Id, option.Name, option.CatalogSubType.Id, option.Description, option.Quantifier, option.WeightInGram, option.Csr, option.EditorId, option.ImageUri); // TRICKED ITEM ID

        _context.CatalogOption.Add(item);

        await _context.SaveChangesAsync();

        return CreatedAtAction(nameof(CreateCatalogOption), new { id = item.Id }, null);
      }
      catch (Exception ex)
      {
        return BadRequest(new { Message = ex.Message });
      }
    }

    // // GET: api/CatalogOption/5
    // [HttpGet("{id}")]
    // public async Task<ActionResult<CatalogOption>> GetCatalogOption(int id)
    // {
    //     var catalogOption = await _context.CatalogOption.FindAsync(id);

    //     if (catalogOption == null)
    //     {
    //         return NotFound();
    //     }

    //     return catalogOption;
    // }

    // // PUT: api/CatalogOption/5
    // // To protect from overposting attacks, enable the specific properties you want to bind to, for
    // // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    // [HttpPut("{id}")]
    // public async Task<IActionResult> PutCatalogOption(int id, CatalogOption catalogOption)
    // {
    //     if (id != catalogOption.Id)
    //     {
    //         return BadRequest();
    //     }

    //     _context.Entry(catalogOption).State = EntityState.Modified;

    //     try
    //     {
    //         await _context.SaveChangesAsync();
    //     }
    //     catch (DbUpdateConcurrencyException)
    //     {
    //         if (!CatalogOptionExists(id))
    //         {
    //             return NotFound();
    //         }
    //         else
    //         {
    //             throw;
    //         }
    //     }

    //     return NoContent();
    // }

    // // DELETE: api/CatalogOption/5
    // [HttpDelete("{id}")]
    // public async Task<ActionResult<CatalogOption>> DeleteCatalogOption(int id)
    // {
    //     var catalogOption = await _context.CatalogOption.FindAsync(id);
    //     if (catalogOption == null)
    //     {
    //         return NotFound();
    //     }

    //     _context.CatalogOption.Remove(catalogOption);
    //     await _context.SaveChangesAsync();

    //     return catalogOption;
    // }

    private bool CatalogOptionExists(int id)
    {
      return _context.CatalogOption.Any(e => e.Id == id);
    }
  }
}
