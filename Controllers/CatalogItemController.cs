using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using MountrashMockAPI.Infrastructure;
using MountrashMockAPI.ViewModel;
using MountrashMockAPI.Models;

namespace MountrashMockAPI.Controllers
{
  [Route("api/v1/[controller]")]
  [ApiController]
  public class CatalogItemController : ControllerBase
  {
    private readonly DatabaseContext _context;

    public CatalogItemController(DatabaseContext context)
    {
      _context = context;
    }

    [HttpGet]
    [Route("global")]
    [ProducesResponseType(typeof(PaginatedListViewModel<CatalogItem>), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> GetCatalogItem([FromQuery]int pageSize = 10, [FromQuery]int pageIndex = 0, CancellationToken cancellationToken = default(CancellationToken))
    {

      try
      {
        var totalItems = await _context.CatalogItem
                .LongCountAsync();

        var itemsOnPage = await _context.CatalogItem
            .OrderBy(c => c.Name)
            .Skip(pageSize * pageIndex)
            .Take(pageSize)
            .ToListAsync();

        var model = new PaginatedListViewModel<CatalogItem>(pageIndex, pageSize, totalItems, itemsOnPage);

        return Ok(model);
      }
      catch (Exception ex)
      {
        return BadRequest(new { Message = ex.Message });
      }
    }

    // POST: api/CatalogItem
    // To protect from overposting attacks, enable the specific properties you want to bind to, for
    // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    [HttpPost]
    public async Task<IActionResult> CreateCatalogItem([FromBody]CatalogItem citem)
    {
      try
      {

        var item = new CatalogItem(citem.Name, citem.Description, citem.EditorId, citem.CatalogOptions, citem.ImageUri);

        _context.CatalogItem.Add(item);

        await _context.SaveChangesAsync();

        return CreatedAtAction(nameof(CreateCatalogItem), new { id = item.Id }, null);
      }
      catch (Exception ex)
      {
        return BadRequest(new { Message = ex.Message });
      }
    }

    // // GET: api/CatalogItem/5
    // [HttpGet("{id}")]
    // public async Task<ActionResult<CatalogItem>> GetCatalogItem(int id)
    // {
    //   var catalogItem = await _context.CatalogItem.FindAsync(id);

    //   if (catalogItem == null)
    //   {
    //     return NotFound();
    //   }

    //   return catalogItem;
    // }

    // // PUT: api/CatalogItem/5
    // // To protect from overposting attacks, enable the specific properties you want to bind to, for
    // // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
    // [HttpPut("{id}")]
    // public async Task<IActionResult> PutCatalogItem(int id, CatalogItem catalogItem)
    // {
    //   if (id != catalogItem.Id)
    //   {
    //     return BadRequest();
    //   }

    //   _context.Entry(catalogItem).State = EntityState.Modified;

    //   try
    //   {
    //     await _context.SaveChangesAsync();
    //   }
    //   catch (DbUpdateConcurrencyException)
    //   {
    //     if (!CatalogItemExists(id))
    //     {
    //       return NotFound();
    //     }
    //     else
    //     {
    //       throw;
    //     }
    //   }

    //   return NoContent();
    // }

    // // DELETE: api/CatalogItem/5
    // [HttpDelete("{id}")]
    // public async Task<ActionResult<CatalogItem>> DeleteCatalogItem(int id)
    // {
    //   var catalogItem = await _context.CatalogItem.FindAsync(id);
    //   if (catalogItem == null)
    //   {
    //     return NotFound();
    //   }

    //   _context.CatalogItem.Remove(catalogItem);
    //   await _context.SaveChangesAsync();

    //   return catalogItem;
    // }

    private bool CatalogItemExists(int id)
    {
      return _context.CatalogItem.Any(e => e.Id == id);
    }
  }
}
