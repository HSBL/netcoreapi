using System;

namespace MountrashMockAPI.Models
{
  public class CatalogLocation
  {
    public int Id { get; set; }
    public string ProvinceName { get; set; }
    public string CountryName { get; set; }

    protected CatalogLocation()
    {

    }

    public CatalogLocation(string provinceName, string countryName) : this()
    {
      ProvinceName = !string.IsNullOrWhiteSpace(provinceName) ? provinceName : throw new ArgumentNullException(nameof(provinceName));
      CountryName = !string.IsNullOrWhiteSpace(countryName) ? countryName : throw new ArgumentNullException(nameof(countryName));
    }
    public void EditLocation(string provinceName, string countryName)
    {
      ProvinceName = !string.IsNullOrWhiteSpace(provinceName) ? provinceName : throw new ArgumentNullException(nameof(provinceName));

      CountryName = !string.IsNullOrWhiteSpace(countryName)
        ? countryName
        : throw new ArgumentNullException(nameof(countryName));
    }
  }
}
