using System;
using MountrashMockAPI.Infrastructure.Exceptions;

namespace MountrashMockAPI.Models
{
  public class CatalogPrice
  {
    private int _priceTypeId;
    private int _locationId;
    private int _catalogSubTypeId;
    public int Id { get; set; }
    public decimal PricePerUnit { get; set; }
    public CatalogLocation Location { get; set; }
    public DateTime IssuedTime { get; set; }
    public CatalogPriceType PriceType { get; set; }
    protected CatalogPrice()
    {

    }

    public CatalogPrice(int catalogSubTypeId, decimal pricePerUnit, int locationId, int priceTypeId) : this()
    {
      _catalogSubTypeId = catalogSubTypeId;

      if (pricePerUnit < 0) throw new CatalogDomainException("pricePerUnit can't be negative");
      PricePerUnit = pricePerUnit;

      _locationId = locationId;
      _priceTypeId = priceTypeId;
      IssuedTime = DateTime.UtcNow;
    }
    public void EditPrice(int catalogSubTypeId, decimal pricePerUnit, int locationId, int priceTypeId)
    {
      _catalogSubTypeId = !catalogSubTypeId.Equals(null) ? catalogSubTypeId : throw new ArgumentNullException(nameof(catalogSubTypeId));

      PricePerUnit = !(PricePerUnit.Equals(null) || PricePerUnit.Equals(0)) ? pricePerUnit : throw new ArgumentNullException(nameof(pricePerUnit));
      _locationId = !locationId.Equals(null) ? locationId : throw new ArgumentNullException(nameof(locationId));
      _priceTypeId = !priceTypeId.Equals(null) ? priceTypeId : throw new ArgumentNullException(nameof(priceTypeId));
    }
  }
}
