using System;
using System.Collections.Generic;
using System.Linq;
using MountrashMockAPI.Infrastructure.Exceptions;

namespace MountrashMockAPI.Models
{
  public class CatalogPriceType
  {
    public static CatalogPriceType TPPrice = new CatalogPriceType(1, nameof(TPPrice).ToLowerInvariant());
    public static CatalogPriceType MPPrice = new CatalogPriceType(2, nameof(MPPrice).ToLowerInvariant());
    public static CatalogPriceType MCPPrice = new CatalogPriceType(3, nameof(MCPPrice).ToLowerInvariant());

    public CatalogPriceType(int id, string name)
    {
      Id = id;
      Name = name;
    }

    public int Id { get; set; }
    public string Name { get; set; }


    public static IEnumerable<CatalogPriceType> List()
    {
      return new[] { TPPrice, MPPrice, MCPPrice };
    }

    public static CatalogPriceType FromName(string name)
    {
      var state = List()
        .SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

      if (state == null)
        throw new CatalogDomainException(
          $"Possible values for CatalogPriceType: {string.Join(",", List().Select(s => s.Name))}");

      return state;
    }

    public static CatalogPriceType From(int id)
    {
      var state = List().SingleOrDefault(s => s.Id == id);

      if (state == null)
        throw new CatalogDomainException(
          $"Possible values for CatalogPriceType: {string.Join(",", List().Select(s => s.Name))}");

      return state;
    }
  }
}
