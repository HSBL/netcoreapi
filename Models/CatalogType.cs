using Microsoft.EntityFrameworkCore.Internal;
using System;
using MountrashMockAPI.Infrastructure.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace MountrashMockAPI.Models
{
  public class CatalogType
  {
    protected CatalogType()
    {
    }

    public CatalogType(string name, string description, string imageUri,
      List<(string name, string description, string imageUri, List<(decimal price, string name)> catalogPrices, List<int> locationsId)> catalogSubTypes) : this()
    {
      Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));
      Description = !string.IsNullOrWhiteSpace(description)
        ? description
        : throw new ArgumentNullException(nameof(description));

      if (!EnumerableExtensions.Any(catalogSubTypes)) throw new ArgumentNullException(nameof(catalogSubTypes));

      CatalogSubTypes = catalogSubTypes.Select(x => new CatalogSubType(Id, x.name, x.description, x.imageUri, x.catalogPrices, x.locationsId)).ToList();
      ImageUri = imageUri;
    }

    public int Id { get; set; }
    public List<CatalogSubType> CatalogSubTypes { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string ImageUri { get; set; }

    public void EditPrice(int catalogSubTypeId, string priceType, decimal price, int locationIds)
    {
      if (CatalogSubTypes.Any()) throw new CatalogDomainException("Catalog sub Type is empty");

      var subType = CatalogSubTypes.FirstOrDefault(x => x.Id == catalogSubTypeId);

      if (subType == null) throw new CatalogDomainException("Catalog sub Type not found");
      subType.EditPrice(priceType, price, locationIds);
    }

    public void EditType(string name, string description, string imageUri)
    {
      Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));

      Description = !string.IsNullOrWhiteSpace(description)
        ? description
        : throw new ArgumentNullException(nameof(description));
      ImageUri = !string.IsNullOrWhiteSpace(imageUri) ? imageUri : throw new ArgumentNullException(nameof(imageUri));
    }

    public CatalogSubType AddSubType(int catalogTypeId, string name, string description, string imageUri, List<(decimal price, string name)> catalogPrices, List<int> locationsId)
    {
      if (!CatalogSubTypes.Any(x => x.Name == name))
      {
        CatalogSubType subType = new CatalogSubType(catalogTypeId, name, description, imageUri, catalogPrices, locationsId);
        CatalogSubTypes.Add(subType);
        return subType;
      }
      else
      {
        throw new CatalogDomainException("SubType with name {name} already exist");
      }
    }

  }
}
