using System;
using System.Collections.Generic;
using System.Linq;
using MountrashMockAPI.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore.Internal;

namespace MountrashMockAPI.Models
{
  public class CatalogItem
  {
    protected CatalogItem()
    { }
    public CatalogItem(
      string name,
      string description,
      string editorId,
      List<CatalogOption> catalogOpt,
      string imageUri) : this()
    {
      EditItem(name, description, editorId, imageUri);

      if (!EnumerableExtensions.Any(catalogOpt)) throw new ArgumentNullException(nameof(catalogOpt));
      var opts = new List<CatalogOption>();
      opts.AddRange(catalogOpt.Select(x =>
        new CatalogOption(Id, x.Name, x.CatalogSubType.Id, x.Description, x.Quantifier, x.WeightInGram, x.Csr, editorId, x.ImageUri)));
      CatalogOptions = opts;

      EditorId = editorId;

      LastUpdated = DateTime.UtcNow;
    }

    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string ImageUri { get; set; }
    public List<CatalogOption> CatalogOptions { get; set; }
    public DateTime LastUpdated { get; set; }
    public string EditorId { get; set; }

    public void EditItem(string name, string description, string editorId, string imageUri)
    {
      Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));

      Description = !string.IsNullOrWhiteSpace(description)
        ? description
        : throw new ArgumentNullException(nameof(description));

      ImageUri = !string.IsNullOrWhiteSpace(imageUri) ? imageUri : throw new ArgumentNullException(nameof(imageUri));

      EditorId = !string.IsNullOrWhiteSpace(editorId) ? editorId : throw new ArgumentNullException(nameof(editorId));
      LastUpdated = DateTime.UtcNow;
    }

    public void EditOption(int catalogOptionId, int subTypeId, string name,
      string description, string quantifier, int weightInGram, int csr, string editorId, string imageUri)
    {
      if (CatalogOptions.Any()) throw new CatalogDomainException("Catalog option is empty");
      var option = CatalogOptions.FirstOrDefault(x => x.Id == catalogOptionId);

      if (option == null) throw new CatalogDomainException("catalog option not found");

      option.EditOption(name, subTypeId, description, quantifier, weightInGram, csr, editorId, imageUri);
    }
    public CatalogOption AddOption(int catalogItemId, string name, int subTypeId,
      string description, string quantifier, int weightInGram, int csr, string editorId,
      string imageUri)
    {
      if (!CatalogOptions.Any(x => x.Name == name))
      {
        CatalogOption option = new CatalogOption(catalogItemId, name, subTypeId, description, quantifier, weightInGram, csr, editorId, imageUri);
        CatalogOptions.Add(option);
        return option;
      }
      else
      {
        throw new CatalogDomainException("Catalog Option with name {name} already exist");
      }
    }
    public void EditAllCsr(int csr, string editorId)
    {
      if (CatalogOptions.Any()) throw new CatalogDomainException("Catalog option is empty");
      CatalogOptions.ForEach(opt => opt.EditCsr(csr, editorId));
    }
    public void EditOptionCsr(int catalogOptionId, int csr, string editorId)
    {
      if (CatalogOptions.Any()) throw new CatalogDomainException("Catalog option is empty");

      var option = CatalogOptions.FirstOrDefault(x => x.Id == catalogOptionId);

      if (option == null) throw new CatalogDomainException("catalog option not found");
      option.EditCsr(csr, editorId);
    }
  }
}
