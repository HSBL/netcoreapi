using System;
using System.Collections.Generic;
using System.Linq;
using MountrashMockAPI.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore.Internal;

namespace MountrashMockAPI.Models
{
  public class CatalogSubType
  {

    protected CatalogSubType()
    {
    }

    public CatalogSubType(int catalogTypeId, string name, string description, string imageUri, List<(decimal price, string name)> catalogPrices, List<int> locationsId) : this()
    {
      Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));
      Description = !string.IsNullOrWhiteSpace(description)
        ? description
        : throw new ArgumentNullException(nameof(description));
      if (!EnumerableExtensions.Any(catalogPrices)) throw new ArgumentNullException(nameof(catalogPrices));

      var prices = new List<CatalogPrice>();
      locationsId.ForEach(locationId =>
        {
          prices.AddRange(catalogPrices.Select(x =>
            new CatalogPrice(Id, x.price, locationId, CatalogPriceType.FromName(x.name).Id)));
        });
      CatalogPrices = prices;
      if (!catalogTypeId.Equals(null)) throw new ArgumentNullException(nameof(catalogTypeId));
      CatalogTypeId = !(catalogTypeId <= 0) ? catalogTypeId : throw new ArgumentException("Catalog Type Id can not be negative or zero");
      ImageUri = !string.IsNullOrWhiteSpace(imageUri) ? imageUri : throw new ArgumentNullException(nameof(imageUri));
    }

    public int Id { get; set; }
    public int CatalogTypeId { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string ImageUri { get; set; }
    public List<CatalogPrice> CatalogPrices { get; set; }

    public void EditPrice(string priceType, decimal price, int locationIds)
    {
      if (!CatalogPrices.Any()) throw new CatalogDomainException("Catalog price is empty");
      var priceToEdit = CatalogPrices.FirstOrDefault(x => x.Location.Id == locationIds && x.PriceType.Name == priceType);
      if (priceToEdit == null) throw new CatalogDomainException("catalog price not found");
      priceToEdit.PricePerUnit = price;
    }

    public void EditSubType(int catalogTypeId, string name, string description, string imageUri)
    {
      if (!catalogTypeId.Equals(null)) throw new ArgumentNullException(nameof(catalogTypeId));
      CatalogTypeId = !(catalogTypeId <= 0) ? catalogTypeId : throw new ArgumentException("Catalog Type Id can not be negative or zero");
      Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));

      Description = !string.IsNullOrWhiteSpace(description)
        ? description
        : throw new ArgumentNullException(nameof(description));
      ImageUri = !string.IsNullOrWhiteSpace(imageUri) ? imageUri : throw new ArgumentNullException(nameof(imageUri));
    }
  }
}
