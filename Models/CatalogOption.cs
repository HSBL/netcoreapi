using System;
using System.Collections.Generic;
using System.Linq;
using MountrashMockAPI.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore.Internal;

namespace MountrashMockAPI.Models
{
  public class CatalogOption
  {
    private int _catalogItemId;
    private int _catalogSubTypeId;

    protected CatalogOption()
    {
    }

    public CatalogOption(int catalogItemId, string name, int subTypeId,
      string description, string quantifier, int weightInGram, int csr, string editorId,
      string imageUri) : this()
    {
      Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));
      Description = !string.IsNullOrWhiteSpace(description)
        ? description
        : throw new ArgumentNullException(nameof(description));
      Quantifier = !string.IsNullOrWhiteSpace(quantifier)
        ? quantifier
        : throw new ArgumentNullException(nameof(quantifier));
      WeightInGram = weightInGram > 0 ? weightInGram : throw new CatalogDomainException("weightInGram can't be negative or zero");
      Csr = csr > 0 ? csr : throw new CatalogDomainException("csr can't be negative");
      ImageUri = !string.IsNullOrWhiteSpace(imageUri) ? imageUri : throw new ArgumentNullException(nameof(imageUri));

      EditorId = !string.IsNullOrWhiteSpace(editorId) ? editorId : throw new ArgumentNullException(nameof(editorId));
      LastUpdated = DateTime.UtcNow;

      _catalogSubTypeId = !subTypeId.Equals(null) ? subTypeId : throw new ArgumentNullException(nameof(subTypeId));
      _catalogItemId = !catalogItemId.Equals(null) ? catalogItemId : throw new ArgumentNullException(nameof(catalogItemId));
    }

    public int Id { get; set; }

    public string Name { get; set; }
    public string ImageUri { get; set; }

    public string Description { get; set; }
    public string Quantifier { get; set; }
    public int WeightInGram { get; set; }
    public int Csr { get; set; }
    public CatalogSubType CatalogSubType { get; set; }
    public DateTime LastUpdated { get; set; }
    public string EditorId { get; set; }
    public int GetSubTypeId()
    {
      return _catalogSubTypeId;
    }
    public void EditOption(string name, int subTypeId,
      string description, string quantifier, int weightInGram, int csr, string editorId, string imageUri)
    {
      Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));
      ;

      Description = !string.IsNullOrWhiteSpace(description)
        ? description
        : throw new ArgumentNullException(nameof(description));

      Quantifier = !string.IsNullOrWhiteSpace(quantifier)
        ? quantifier
        : throw new ArgumentNullException(nameof(quantifier));

      ImageUri = !string.IsNullOrWhiteSpace(imageUri) ? imageUri : throw new ArgumentNullException(nameof(imageUri));

      WeightInGram = weightInGram > 0 ? weightInGram : throw new CatalogDomainException("weightInGram can't be negative or zero");

      Csr = csr > 0 ? csr : throw new CatalogDomainException("csr can't be negative");

      _catalogSubTypeId = !subTypeId.Equals(null) ? subTypeId : throw new ArgumentNullException(nameof(subTypeId));

      EditorId = !string.IsNullOrWhiteSpace(editorId) ? editorId : throw new ArgumentNullException(nameof(editorId));

      LastUpdated = DateTime.UtcNow;
    }
    public void EditCsr(int csr, string editorId)
    {
      Csr = csr > 0 ? csr : throw new CatalogDomainException("csr can't be negative");

      EditorId = !string.IsNullOrWhiteSpace(editorId) ? editorId : throw new ArgumentNullException(nameof(editorId));

      LastUpdated = DateTime.UtcNow;
    }
  }
}
