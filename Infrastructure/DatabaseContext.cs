using Microsoft.EntityFrameworkCore;
using MountrashMockAPI.Models;

namespace MountrashMockAPI.Infrastructure
{
  public class DatabaseContext : DbContext
  {
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
    { }
    public DbSet<CatalogItem> CatalogItem { get; set; }
    public DbSet<CatalogOption> CatalogOption { get; set; }
  }
}